﻿namespace Kelly.Shared.Models {
    public class Order {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public Payment Payment { get; set; }
    }
}