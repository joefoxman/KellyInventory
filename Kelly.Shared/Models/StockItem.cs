﻿namespace Kelly.Shared.Models {
    public class StockItem {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
    }
}