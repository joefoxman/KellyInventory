﻿namespace Kelly.Shared.Models {
    public class Payment {
        public int Id { get; set; }
        public string CardNumber { get; set; }
        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }
        public int CcvCode { get; set; }
        public decimal AmountToCharge { get; set; }
    }
}