﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Kelly.Contracts.Repositories;
using Kelly.Shared.Constants;
using Kelly.Shared.Models;

namespace Kelly.Repositories{
    public class KellyDbContext : DbContext, IDbContext {
        public KellyDbContext() : base(Constants.EntitiesConnectionStringName){
            Database.SetInitializer<KellyDbContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<Order>().ToTable("Order");
        }
    }
}