﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Kelly.Contracts.Repositories;
using EntityState = System.Data.Entity.EntityState;

namespace Kelly.Repositories.Repositories{
    public class GenericRepository<T> : IBaseRepository<T> where T : class{
        protected readonly DbContext Context;
        protected readonly DbSet<T> DbSet;

        public GenericRepository(IDbContext dbContext){
            Context = (DbContext) dbContext;
            if (Context.Database.Connection.State != ConnectionState.Open)
                Context.Database.Connection.Open();
            // DO NOT enable proxied entities, else serialization fails
            Context.Configuration.ProxyCreationEnabled = false;
            // Load navigation properties explicitly (avoid serialization trouble)
            Context.Configuration.LazyLoadingEnabled = false;
            // Because we will perform validation, we don't need/want EF to do so
            Context.Configuration.ValidateOnSaveEnabled = false;
            DbSet = Context.Set<T>();
        }

        public virtual IEnumerable<T> Find(Func<T, bool> predicate){
            var entities = DbSet.Where(predicate).ToList();
            foreach (var entity in entities){
                Context.Entry(entity).State = EntityState.Detached;
            }
            return entities;
        }

        public virtual T Insert(T entity){
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            // Add to database
            DbSet.Add(entity);
            Context.SaveChanges();
            return entity;
        }

        public bool Exists(T entity) {
            return Context.Set<T>().Local.Any(e => e == entity);
        }

        public virtual T Update(T entity){
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            // Add to database
            if (!Exists(entity)){
                DbSet.Attach(entity);
            }
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
            return entity;
        }

        public virtual T GetById(int id){
            if (id == 0){
                return null;
            }
            var entity = DbSet.Find(id);
            if (entity == null) return null;
            // Detach the entity.  We don't use it in this context.
            var dbEntityEntry = Context.Entry(entity);
            dbEntityEntry.State = EntityState.Detached;
            return entity;
        }

        public virtual IEnumerable<T> GetAll(){
            return DbSet;
        }

        public virtual void Delete(T entity){
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            try{
                Context.Entry(entity).State = EntityState.Deleted;
                DbSet.Remove(entity);

                var changes = Context.SaveChanges();
                if (changes == 0)
                    throw new OptimisticConcurrencyException();
            }
            catch (OptimisticConcurrencyException){
                throw new OptimisticConcurrencyException();
            }
        }

        protected virtual T _getByIdFromDb(int id){
            return DbSet.Find(id);
        }

        #region IDisposable Members

        public void Dispose(){
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing){
            if (disposing){
            }
        }

        #endregion
    }
}