﻿using System.Collections.Generic;
using System.Linq;
using Kelly.Contracts.Repositories;
using Kelly.Shared.Models;

namespace Kelly.Repositories.Repositories{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository {
        // TODO: This data needs to come from a DB in the future
        public IEnumerable<StockItem> StockItems { get; set; }

        public OrderRepository(IDbContext context) : base(context){
        }

        public bool IsInStock(Order order) {
            // TODO: Call persistance store (DB) to see if this order item is in stock
            var stockItem = StockItems.FirstOrDefault(a => a.Id.Equals(order.Id));
            return stockItem?.Quantity >= order.Quantity;
        }

        public int DecrementStock(Order order) {
            var stockItem = StockItems.FirstOrDefault(a => a.Id.Equals(order.Id));
            if (stockItem == null) {
                return 0;
            }
            // TODO: Need to architect concurrency and put a possible hold on items
            // otherwise stock levels could go below zero
            stockItem.Quantity -= order.Quantity;
            // return current stock quantity
            return stockItem.Quantity;
        }
    }
}