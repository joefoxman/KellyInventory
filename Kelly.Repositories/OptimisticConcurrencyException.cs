﻿using System;
using System.Runtime.Serialization;

namespace Kelly.Repositories{
    public class OptimisticConcurrencyException : Exception{
        public OptimisticConcurrencyException(){
        }

        public OptimisticConcurrencyException(string entityName, string key){
            EntityName = entityName;
            Key = key;
        }

        public OptimisticConcurrencyException(string entityName, string key, string message)
            : base(message){
            EntityName = entityName;
            Key = key;
        }

        public OptimisticConcurrencyException(string entityName, string key, string message, Exception innerException)
            : base(message, innerException){
            EntityName = entityName;
            Key = key;
        }

        public OptimisticConcurrencyException(string entityName, string key, SerializationInfo info,
            StreamingContext context)
            : base(info, context){
            EntityName = entityName;
            Key = key;
        }

        public string Key { get; private set; }
        public string EntityName { get; private set; }
    }
}