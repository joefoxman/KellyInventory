﻿using System.Collections.Generic;
using Kelly.Shared.Models;

namespace Kelly.Repositories.Data {
    public class OrderData {
        public IEnumerable<Order> Orders { get; set; }
    }
}