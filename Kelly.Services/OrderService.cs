﻿using System;
using Kelly.Contracts.Repositories;
using Kelly.Contracts.Services;
using Kelly.Shared.Models;

namespace Kelly.Services {
    public class OrderService : IOrderService {
        private readonly IOrderRepository _orderRepository;
        private readonly IPaymentService _paymentService;
        private readonly IEmailService _emailService;

        public OrderService(
            IOrderRepository orderRepository,
            IPaymentService paymentService,
            IEmailService emailService) {
            _orderRepository = orderRepository;
            _paymentService = paymentService;
            _emailService = emailService;
        }

        public bool Process(Order order) {
            // check inventory
            if (!CheckInventory(order)) {
                return false;
            }
            // verify credit card
            var paymentVerified = _paymentService.VerifyCreditCard(order.Payment);
            if (!paymentVerified) {
                return false;
            }
            // process order
            if (!_paymentService.ChargePayment(order.Payment)) {
                return false;
            }
            // decrement count of items by number in order
            _orderRepository.DecrementStock(order);

            // TODO: Get email Message from template
            var emailMessage = new EmailMessage();
            return NotifyShippingDepartment(order, emailMessage);
        }

        public bool CheckInventory(Order order) {
            var isInStock = _orderRepository.IsInStock(order);
            return isInStock;
        }

        public bool NotifyShippingDepartment(Order order, EmailMessage emailMessage) {
            return _emailService.SendEmail(emailMessage);
        }
    }
}