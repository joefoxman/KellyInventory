﻿using System.Collections.Generic;
using Kelly.Shared.Models;

namespace Kelly.Contracts.Repositories {
    public interface IOrderRepository {
        IEnumerable<StockItem> StockItems { get; set; }
        bool IsInStock(Order order);
        int DecrementStock(Order order);
    }
}
