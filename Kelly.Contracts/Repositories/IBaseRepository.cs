﻿using System;
using System.Collections.Generic;

namespace Kelly.Contracts.Repositories{
    public interface IBaseRepository<T> : IDisposable
        where T : class{
        IEnumerable<T> GetAll();
        T GetById(int id);
        IEnumerable<T> Find(Func<T, bool> predicate);
        T Insert(T entity);
        T Update(T entity);
    }
}