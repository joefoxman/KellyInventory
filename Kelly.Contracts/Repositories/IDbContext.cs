﻿namespace Kelly.Contracts.Repositories{
    public interface IDbContext{
        void Dispose();
    }
}