﻿using Kelly.Shared.Models;

namespace Kelly.Contracts.Services{
    public interface IOrderService{
        bool Process(Order order);
        bool CheckInventory(Order order);
        bool NotifyShippingDepartment(Order order, EmailMessage emailMessage);
    }
}