﻿using Kelly.Shared.Models;

namespace Kelly.Contracts.Services {
    public interface IEmailService {
        bool SendEmail(EmailMessage emailMessage);
    }
}
