﻿using Kelly.Shared.Models;

namespace Kelly.Contracts.Services {
    public interface IPaymentService {
        bool VerifyCreditCard(Payment payment);
        bool ChargePayment(Payment payment);
    }
}