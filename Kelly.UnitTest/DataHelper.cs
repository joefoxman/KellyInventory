﻿using System.Collections.Generic;
using Kelly.Shared.Models;

namespace Kelly.UnitTests {
    public class DataHelper {
        public static Order GetOrder() {
            var order = new Order {
                Id = 1,
                Quantity = 5,
                Payment =
                    new Payment {
                        CardNumber = "1111111111111111",
                        ExpirationMonth = 12,
                        ExpirationYear = 2020,
                        CcvCode = 123
                    }
            };
            return order;
        }

        public static IEnumerable<StockItem> GetStockItems() {
            var orders = new List<StockItem> {
                new StockItem {
                    Id = 1,
                    Description = "Item 1",
                    Quantity = 5
                },
                new StockItem {
                    Id = 2,
                    Description = "Item 2",
                    Quantity = 12
                }
            };
            return orders;
        }
    }
}