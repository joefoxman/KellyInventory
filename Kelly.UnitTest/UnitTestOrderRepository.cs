﻿using System.Linq;
using Kelly.Contracts.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Kelly.UnitTests {
    [TestClass]
    public class UnitTestOrderRepository : TransactionalTest {
        [TestMethod]
        public void OrderRepositoryDecrementStockTrue() {
            var stockItems = DataHelper.GetStockItems().ToList();
            var orderRepo = new Mock<IOrderRepository>();
            var order = DataHelper.GetOrder();
            var stockItem = stockItems.FirstOrDefault(a => a.Id.Equals(order.Id));
            orderRepo.Setup(a => a.StockItems).Returns(stockItems);
            if (stockItem == null) {
                Assert.Fail("Stock Item is Null");
            }
            orderRepo.Setup(a => a.DecrementStock(order)).Returns(stockItem.Quantity - order.Quantity);
            stockItem.Quantity = orderRepo.Object.DecrementStock(order);
            Assert.IsTrue(stockItem.Quantity == 0);
        }

        [TestMethod]
        public void OrderRepositoryDecrementStockFalse() {
            var stockItems = DataHelper.GetStockItems().ToList();
            var orderRepo = new Mock<IOrderRepository>();
            var order = DataHelper.GetOrder();
            var stockItem = stockItems.FirstOrDefault(a => a.Id.Equals(order.Id));
            orderRepo.Setup(a => a.StockItems).Returns(stockItems);
            if (stockItem == null) {
                Assert.Fail("Stock Item is Null");
            }
            orderRepo.Setup(a => a.DecrementStock(order)).Returns(stockItem.Quantity - order.Quantity - 1);
            stockItem.Quantity = orderRepo.Object.DecrementStock(order);
            Assert.IsTrue(stockItem.Quantity < 0);
        }

        [TestMethod]
        public void OrderRepositoryIsInStockTrue() {
            var stockItems = DataHelper.GetStockItems();
            var orderRepo = new Mock<IOrderRepository>();
            var order = DataHelper.GetOrder();
            orderRepo.Setup(a => a.StockItems).Returns(stockItems);
            orderRepo.Setup(a => a.IsInStock(order)).Returns(true);
            var isInStock = orderRepo.Object.IsInStock(order);
            Assert.IsTrue(isInStock);
        }

        [TestMethod]
        public void OrderRepositoryIsInStockFalse() {
            var stockItems = DataHelper.GetStockItems();
            var orderRepo = new Mock<IOrderRepository>();
            var order = DataHelper.GetOrder();
            orderRepo.Setup(a => a.StockItems).Returns(stockItems);
            orderRepo.Setup(a => a.IsInStock(order)).Returns(false);
            var isInStock = orderRepo.Object.IsInStock(order);
            Assert.IsFalse(isInStock);
        }
    }
}