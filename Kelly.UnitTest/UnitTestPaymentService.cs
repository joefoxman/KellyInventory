﻿using Kelly.Contracts.Services;
using Kelly.Shared.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Kelly.UnitTests {
    [TestClass]
    public class UnitTestPaymentService {
        private static Payment GetPayment() {
            var payment = new
                Payment {
                    Id = 1,
                    CardNumber = "1111111111111111",
                    ExpirationMonth = 12,
                    ExpirationYear = 2020,
                    CcvCode = 123,
                    AmountToCharge = 145.60M
                };
            return payment;
        }

        [TestMethod]
        public void VerifyCreditCardTrue() {
            var payment = GetPayment();
            var paymentService = new Mock<IPaymentService>();
            paymentService.Setup(a => a.VerifyCreditCard(payment)).Returns(true);
            var creditCardVerified = paymentService.Object.VerifyCreditCard(payment);
            Assert.IsTrue(creditCardVerified);
        }

        [TestMethod]
        public void VerifyCreditCardFalse() {
            var payment = GetPayment();
            var paymentService = new Mock<IPaymentService>();
            paymentService.Setup(a => a.VerifyCreditCard(payment)).Returns(false);
            var creditCardVerified = paymentService.Object.VerifyCreditCard(payment);
            Assert.IsFalse(creditCardVerified);
        }

        [TestMethod]
        public void ChargePaymentTrue() {
            var payment = GetPayment();
            var paymentService = new Mock<IPaymentService>();
            paymentService.Setup(a => a.ChargePayment(payment)).Returns(true);
            var paymentCharged = paymentService.Object.ChargePayment(payment);
            Assert.IsTrue(paymentCharged);
        }

        [TestMethod]
        public void ChargePaymentFalse() {
            var payment = GetPayment();
            var paymentService = new Mock<IPaymentService>();
            paymentService.Setup(a => a.ChargePayment(payment)).Returns(false);
            var paymentCharged = paymentService.Object.ChargePayment(payment);
            Assert.IsFalse(paymentCharged);
        }
    }
}