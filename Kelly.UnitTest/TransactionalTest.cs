﻿using System.Transactions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kelly.UnitTests{
    public class TransactionalTest{
        protected TransactionScope Ts;

        [TestCleanup]
        public void OnCleanup(){
            Ts.Dispose();
        }

        [TestInitialize]
        public void OnInitialize(){
            Ts = new TransactionScope();
        }
    }
}