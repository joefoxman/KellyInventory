﻿using Kelly.Contracts.Repositories;
using Kelly.Contracts.Services;
using Kelly.Shared.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Kelly.UnitTests {
    [TestClass]
    public class UnitTestOrderService {
        [TestMethod]
        public void OrderServiceIsInStockTrue() {
            var stockItems = DataHelper.GetStockItems();
            var orderRepo = new Mock<IOrderRepository>();
            var orderService = new Mock<IOrderService>();
            var order = DataHelper.GetOrder();
            orderRepo.Setup(a => a.StockItems).Returns(stockItems);
            orderService.Setup(a => a.CheckInventory(order)).Returns(true);
            var isInStock = orderService.Object.CheckInventory(order);
            Assert.IsTrue(isInStock);
        }

        [TestMethod]
        public void OrderServiceIsInStockFalse() {
            var stockItems = DataHelper.GetStockItems();
            var orderRepo = new Mock<IOrderRepository>();
            var orderService = new Mock<IOrderService>();
            var order = DataHelper.GetOrder();
            orderRepo.Setup(a => a.StockItems).Returns(stockItems);
            orderService.Setup(a => a.CheckInventory(order)).Returns(false);
            var isInStock = orderService.Object.CheckInventory(order);
            Assert.IsFalse(isInStock);
        }


        [TestMethod]
        public void NotifyShippingDepartmentTrue() {
            var order = DataHelper.GetOrder();
            var emailMessage = new EmailMessage();
            var orderService = new Mock<IOrderService>();
            orderService.Setup(a => a.NotifyShippingDepartment(order, emailMessage)).Returns(true);
            var notificationSent = orderService.Object.NotifyShippingDepartment(order, emailMessage);
            Assert.IsTrue(notificationSent);
        }

        [TestMethod]
        public void NotifyShippingDepartmentFalse() {
            var order = DataHelper.GetOrder();
            var emailMessage = new EmailMessage();
            var orderService = new Mock<IOrderService>();
            orderService.Setup(a => a.NotifyShippingDepartment(order, emailMessage)).Returns(false);
            var notificationSent = orderService.Object.NotifyShippingDepartment(order, emailMessage);
            Assert.IsFalse(notificationSent);
        }

        [TestMethod]
        public void CheckInventoryTrue() {
            var order = DataHelper.GetOrder();
            var orderService = new Mock<IOrderService>();
            orderService.Setup(a => a.CheckInventory(order)).Returns(true);
            var checkInventory = orderService.Object.CheckInventory(order);
            Assert.IsTrue(checkInventory);
        }

        [TestMethod]
        public void CheckInventoryFalse() {
            var order = DataHelper.GetOrder();
            var orderService = new Mock<IOrderService>();
            orderService.Setup(a => a.CheckInventory(order)).Returns(false);
            var checkInventory = orderService.Object.CheckInventory(order);
            Assert.IsFalse(checkInventory);
        }
    }
}